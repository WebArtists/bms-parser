/*****************************************
FUNCTIONS
******************************************/

exports.send = function(socket,msg){
	socket.write(msg);
	console.log("Sending to "+socket.name+": "+msg);
};

exports.str_pad = function(input, length, string) {
	string = string || '0'; input = input + '';
	return input.length >= length ? input : new Array(length - input.length + 1).join(string) + input;
};

exports.get_date_time = function(date, time) {
	var datetime = "20"+date.substr(6,2)+"/"+date.substr(3,2)+"/"+date.substr(0,2);
	datetime += " " + time + "Z";
	datetime = new Date(datetime);
	return datetime;
};

exports.parse_boolean = function(string){
	switch(string.toLowerCase()){
		case "true": case "yes": case "1": case "on": return true;
		case "false": case "no": case "0": case "off": case null: return false;
		default: return Boolean(string);
	}
};