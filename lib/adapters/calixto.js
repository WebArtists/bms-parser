/*****************************************
CALIXTO ADAPTER
******************************************/

functions = require("../functions");

exports.protocol="BMS101";
exports.model_name="calixto";
exports.compatible_hardware=["calixto/supplier"];

var adapter = function(device) {
	if(!(this instanceof adapter)) {
		return new adapter(device);
	}

	this.format = {"start":"^","end":"#","separator":"|"};
	this.device = device;

	/*******************************************
	PARSE THE INCOMING STRING FROM THE DECIVE
	You must return an object with a least: device_id, cmd and type.
	return device_id: The device_id
	return cmd: command from the device.
	return type: login_request, ping, etc.
	*******************************************/
	this.parse_data = function(data) {
		data = data.toString();
		var cmd_start = data.indexOf(this.format.start), //all the incomming messages has a cmd starting with '^'
			dataParts = data.split(this.format.separator),
			startPart = dataParts[0],
			endPart = dataParts[dataParts.length-1];

		var parts={
			"start"     : startPart.substr(0,1),
			"cmd"       : startPart.substr(1, dataParts[0].length-1), //mandatory
			"device_id" : dataParts[1],//mandatory
			"data"      : dataParts.slice(2, dataParts.length-1),
			"finish"    : endPart
		};

		switch(parts.cmd) {
			case "TMSRT":
				parts.action="login_request";
				break;
			case "CALIXTO":
				parts.action="ping";
				break;
			case "TMALT":
				parts.action="alert";
				break;
			default:
				parts.action="other";
		}

		return parts;
	};

	this.authorize =function() {
		//@TODO: Implement this as and when required
	};

	this.run_other = function(cmd,msg_parts) {
		//@TODO: Implement other functions as and when required
	};

	this.request_login_to_device = function() {
		//@TODO: Implement this as and when required
	};

	this.receive_connection = function(msg_parts) {
		//@TODO: Implement this as and when required
	};

	this.receive_alert = function(msg_parts) {
		//@TODO: Implement this as and when required
		return { code: "not_implemented", msg: "Not implemented yet" };
	};

	this.get_ping_data = function(msg_parts) {
		var arr = msg_parts.data;
		var res = {
			"latitude"                 : parseFloat(arr[0]),
			"longitude"                : parseFloat(arr[1]),
			"time"                     : arr[2],
			"date"                     : arr[3],
			"temperature"              : parseFloat(arr[4]),
			"flow"                     : parseFloat(arr[5]),
			"humidity"                 : parseFloat(arr[6]),
			"occupancy"                : functions.parse_boolean(arr[7]),
			"light_1_status"           : arr[8],
			"light_2_status"           : arr[9],
			"switch_1_status"          : arr[10],
			"switch_2_status"          : arr[11],
			"power"                    : parseFloat(arr[12]),
			"voltage"                  : parseFloat(arr[13]),
			"current"                  : parseFloat(arr[14]),
			"power_factor"             : parseFloat(arr[15]),
			"packet_live_stored_status": functions.parse_boolean(arr[16]),
			"light_1_status_bool"      : functions.parse_boolean(arr[8]),
			"light_2_status_bool"      : functions.parse_boolean(arr[9]),
			"switch_1_status_bool"     : functions.parse_boolean(arr[10]),
			"switch_2_status_bool"     : functions.parse_boolean(arr[11])
		};
		res.datetime = functions.get_date_time(res.date, res.time);
		return res;
	};

	/* SET REFRESH TIME */
	this.set_refresh_time = function(interval,duration) {
		//XXXXYYZZ
		//XXXX Hex interval for each message in seconds
		//YYZZ Total time for feedback
		//YY Hex hours
		//ZZ Hex minutes
		var hours = parseInt(duration/3600);
		var minutes = parseInt((duration-hours*3600)/60);
		var time = f.str_pad(interval.toString(16),4,'0')+ f.str_pad(hours.toString(16),2,'0')+ f.str_pad(minutes.toString(16),2,'0')
		this.send_comand("AR00",time);
	};

	/* INTERNAL FUNCTIONS */

	this.send_comand = function(cmd,data) {
		var msg = [this.device.uid,cmd,data];
		this.device.send(this.format_data(msg));
	};

	this.format_data = function(params) {
		/* FORMAT THE DATA TO BE SENT */
		var str = this.format.start;
		if(typeof(params) == "string") {
			str+=params
		}else if(params instanceof Array) {
			str += params.join(this.format.separator);
		}else{
			throw "The parameters to send to the device has to be a string or an array";
		}
		str+= this.format.end;
		return str;
	};
};

exports.adapter = adapter;
